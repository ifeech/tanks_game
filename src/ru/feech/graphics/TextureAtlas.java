package ru.feech.graphics;

import ru.feech.utils.ResourceLoader;

import java.awt.image.BufferedImage;

/**
 * Работа с изображениями
 */
public class TextureAtlas {

    private BufferedImage image;

    public TextureAtlas(String imageName) {
        image = ResourceLoader.loadImage(imageName);
    }

    public BufferedImage cut(int x, int y, int w, int h) {
        return image.getSubimage(x, y, w, h);
    }
}

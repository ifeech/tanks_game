package ru.feech.graphics;

import java.awt.image.BufferedImage;

/**
 * Работа с картинкой спрайта
 */
public class SpriteSheet {

    private BufferedImage sheet;
    private int spriteCount;
    private int scale; // размер спрайта
    private int spritesInWidth; // кол-во спрайтов в ширину

    public SpriteSheet(BufferedImage sheet, int spriteCount, int scale){
        this.sheet = sheet;
        this.spriteCount = spriteCount;
        this.scale = scale;
        this.spritesInWidth = sheet.getWidth() / scale;
    }

    public BufferedImage getSprite(int index){
        index = index % spriteCount; // сбрасывает индекс (если он больше спрайтов)
        int x = index % spritesInWidth * scale;
        int y = index / spritesInWidth * scale;

        return sheet.getSubimage(x, y, scale, scale);
    }
}

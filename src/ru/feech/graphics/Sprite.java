package ru.feech.graphics;

import ru.feech.utils.Utils;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Отрисовка спрайта
 */
public class Sprite {

    private SpriteSheet sheet;
    private float scale; // коэфф. изменения картинки
    private BufferedImage image;

    public Sprite(SpriteSheet sheet, float scale) {
        this.sheet = sheet;
        this.scale = scale;
        this.image = sheet.getSprite(0);
        image = Utils.resize(image, (int) (image.getWidth() * scale), (int) (image.getHeight() * scale));
    }

    public void render(Graphics2D g, float x, float y) {
        g.drawImage(image, (int) (x), (int) (y), null);
    }
}

package ru.feech.game;

import ru.feech.IO.Input;
import ru.feech.graphics.Sprite;
import ru.feech.graphics.SpriteSheet;
import ru.feech.graphics.TextureAtlas;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Евгений on 11.08.2016.
 */
public class Player extends Entity {

    public static final int SPRITE_SCALE = 16;
    public static final int SPRITES_PER_HEADING = 1;

    /* Сторона обзора */
    private enum Heading {
        NORTH(0 * SPRITE_SCALE, 0 * SPRITE_SCALE, 1 * SPRITE_SCALE, 1 * SPRITE_SCALE),
        EAST(6 * SPRITE_SCALE, 0 * SPRITE_SCALE, 1 * SPRITE_SCALE, 1 * SPRITE_SCALE),
        SOUTH(4 * SPRITE_SCALE, 0 * SPRITE_SCALE, 1 * SPRITE_SCALE, 1 * SPRITE_SCALE),
        WEST(2 * SPRITE_SCALE, 0 * SPRITE_SCALE, 1 * SPRITE_SCALE, 1 * SPRITE_SCALE);

        private int x, y, w, h;

        Heading(int x, int y, int w, int h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        }

        protected BufferedImage texture(TextureAtlas atlas) {
            return atlas.cut(x, y, w, h);
        }
    }

    private Heading heading;
    private Map<Heading, Sprite> spriteMap;
    private float scale;
    private float speed;

    public Player(float x, float y, float scale, float speed, TextureAtlas atlas) {
        super(EntityType.Player, x, y);

        heading = Heading.NORTH;
        spriteMap = new HashMap<Heading, Sprite>();
        this.scale = scale;
        this.speed = speed;

        for (Heading h : Heading.values()) {
            SpriteSheet sheet = new SpriteSheet(h.texture(atlas), SPRITES_PER_HEADING, SPRITE_SCALE);
            Sprite sprite = new Sprite(sheet, scale);
            spriteMap.put(h, sprite);
        }
    }

    @Override
    public void update(Input input) {
        float newX = x;
        float newY = y;

        if(input.getKey(KeyEvent.VK_UP)){
            newY -= speed;
            heading = heading.NORTH;
        }else if(input.getKey(KeyEvent.VK_RIGHT)){
            newX += speed;
            heading = heading.EAST;
        }
        else if(input.getKey(KeyEvent.VK_DOWN)){
            newY += speed;
            heading = heading.SOUTH;
        }else if(input.getKey(KeyEvent.VK_LEFT)){
            newX -= speed;
            heading = heading.WEST;
        }

        x = (newX < 0) ? 0 : (newX >= Game.WIDTH - SPRITE_SCALE * scale) ? Game.WIDTH - SPRITE_SCALE * scale : newX;
        y = (newY < 0) ? 0 : (newY >= Game.HEIGHT - SPRITE_SCALE * scale) ? Game.HEIGHT - SPRITE_SCALE * scale : newY;
    }

    @Override
    public void render(Graphics2D g) {
        spriteMap.get(heading).render(g, x, y);
    }
}

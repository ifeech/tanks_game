package ru.feech.game;

import ru.feech.IO.Input;
import ru.feech.display.Display;
import ru.feech.game.level.Level;
import ru.feech.graphics.Sprite;
import ru.feech.graphics.SpriteSheet;
import ru.feech.graphics.TextureAtlas;
import ru.feech.utils.Time;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Движек
 */
public class Game implements Runnable {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    public static final String TITLE = "Tanks";
    public static final int CLEAR_COLOR = 0xFF000000;
    public static final int MIN_BUFFERS = 3;

    public static final float UPDATE_RATE = 60.0f;
    public static final float UPDATE_INTERVAL = Time.SECOND / UPDATE_RATE; // время м/у update
    public static final long IDLE_TIME = 1; // ожидание потока
    public static final String ATLAS_FILE_NAME = "texture_atlas.png";

    private boolean running;
    private Thread gameThread;
    private Graphics2D graphics;
    private Input input;
    private TextureAtlas atlas;
    private Player player;
    private Level lvl;

    public Game() {
        running = false;
        Display.create(WIDTH, HEIGHT, TITLE, CLEAR_COLOR, MIN_BUFFERS);
        graphics = Display.getGraphics();
        input = new Input();
        Display.addInputListener(input);
        atlas = new TextureAtlas(ATLAS_FILE_NAME);
        player = new Player(300, 300, 2, 3, atlas);
        lvl = new Level(atlas);
    }

    public synchronized void start() {
        if (running) return;
        running = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public synchronized void stop() {
        if (!running) return;
        running = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cleanUp();
    }

    private void update() {
        lvl.update();
        player.update(input);
    }

    private void render() {
        Display.clear();
        lvl.render(graphics);
        player.render(graphics);
        Display.swapBuffers();
    }

    @Override
    public void run() {

        int fps = 0;
        int updateCount = 0;
        int updateLoop = 0;

        long allElapsedTime = 0;

        float delta = 0;
        long lastTime = Time.get(); // время прошлой итерации
        while (running) {
            long nowTime = Time.get();
            long elapsedTime = nowTime - lastTime; // прошло времени с последнего прохода
            lastTime = nowTime;

            allElapsedTime += elapsedTime;

            boolean render = false;
            delta += (elapsedTime / UPDATE_INTERVAL); // к-я 1 update 
            while (delta > 1) {
                update();
                updateCount++;
                delta--;
                if (render) {
                    updateLoop++;
                } else {
                    render = true;
                }
            }
            if (render) {
                render();
                fps++;
            } else {
                try {
                    Thread.sleep(IDLE_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (allElapsedTime >= Time.SECOND) {
                Display.setTitle(TITLE + " || FPS: " + fps + " | UPDATE: " + updateCount + " | UPDATE_LOOP: " + updateLoop);
                fps = 0;
                updateCount = 0;
                updateLoop = 0;
                allElapsedTime = 0;
            }
        }
    }

    private void cleanUp() {
        Display.destroy();
    }
}

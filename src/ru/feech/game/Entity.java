package ru.feech.game;

import ru.feech.IO.Input;

import java.awt.*;

/**
 * Базовый класс сущности
 */
public abstract class Entity {

    public final EntityType type;

    public float x;
    public float y;

    public Entity(EntityType type, float x, float y) {
        this.type = type;
        this.x = x;
        this.y = y;
    }

    protected abstract void update(Input input);
    protected abstract void render(Graphics2D g);
}

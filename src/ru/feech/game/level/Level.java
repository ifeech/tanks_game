package ru.feech.game.level;

import ru.feech.game.Game;
import ru.feech.graphics.TextureAtlas;
import ru.feech.utils.Utils;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Создание уровня
 */
public class Level {

    private static final int TILE_SCALE = 8; //размер tile
    private static final int TILE_IN_GAME_SCALE = 2;
    private static final int SCALED_TILE_SIZE = TILE_SCALE * TILE_IN_GAME_SCALE;
    private static final int TILES_IN_WIDTH = Game.WIDTH / SCALED_TILE_SIZE;
    private static final int TILES_IN_HEIGHT = Game.HEIGHT / SCALED_TILE_SIZE;

    private Integer[][] tileMap; // кол-во tile в w и h
    private Map<TileType, Tile> tiles;

    public Level(TextureAtlas atlas) {
        tiles = new HashMap<>();
        tiles.put(TileType.BRICK, new Tile(
                atlas.cut(32 * TILE_SCALE, 0 * TILE_SCALE, TILE_SCALE, TILE_SCALE),
                TILE_IN_GAME_SCALE,
                TileType.BRICK
        ));
        tiles.put(TileType.METAL, new Tile(
                atlas.cut(32 * TILE_SCALE, 2 * TILE_SCALE, TILE_SCALE, TILE_SCALE),
                TILE_IN_GAME_SCALE,
                TileType.METAL
        ));
        tiles.put(TileType.WATER, new Tile(
                atlas.cut(32 * TILE_SCALE, 4 * TILE_SCALE, TILE_SCALE, TILE_SCALE),
                TILE_IN_GAME_SCALE,
                TileType.WATER
        ));
        tiles.put(TileType.GRASS, new Tile(
                atlas.cut(34 * TILE_SCALE, 4 * TILE_SCALE, TILE_SCALE, TILE_SCALE),
                TILE_IN_GAME_SCALE,
                TileType.GRASS
        ));
        tiles.put(TileType.ICE, new Tile(
                atlas.cut(36 * TILE_SCALE, 4 * TILE_SCALE, TILE_SCALE, TILE_SCALE),
                TILE_IN_GAME_SCALE,
                TileType.ICE
        ));
        tiles.put(TileType.EMPTY, new Tile(
                atlas.cut(36 * TILE_SCALE, 6 * TILE_SCALE, TILE_SCALE, TILE_SCALE),
                TILE_IN_GAME_SCALE,
                TileType.EMPTY
        ));

        tileMap = new Integer[TILES_IN_WIDTH][TILES_IN_HEIGHT];
        tileMap = Utils.levelParser("res/level.lvl");
    }

    public void update() {

    }

    public void render(Graphics2D g) {
        for (int i = 0; i < tileMap.length; i++) {
            for (int j = 0; j < tileMap[i].length; j++) {
                tiles.get(TileType.fromNumeric(tileMap[i][j])).render(g, j * SCALED_TILE_SIZE, i * SCALED_TILE_SIZE);
            }
        }
    }
}

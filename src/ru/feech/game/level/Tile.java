package ru.feech.game.level;

import ru.feech.utils.Utils;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Таблица уровня
 */
public class Tile {

    private BufferedImage image; // img tile
    private TileType type;

    protected Tile(BufferedImage image, int scale, TileType type) {
        this.type = type;
        this.image = Utils.resize(image, image.getWidth() * scale, image.getHeight() * scale);
    }

    protected void render(Graphics2D g, int x, int y) {
        g.drawImage(image, x, y, null);
    }

    // узнать тип tyle
    protected TileType type() {
        return type;
    }
}

package ru.feech.main;

import ru.feech.display.Display;
import ru.feech.game.Game;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Главный класс
 */
public class Main {

    public static void main(String[] args) {

        Game tanks = new Game();
        tanks.start();
    }
}
